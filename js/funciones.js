/**
 * [Recibe jSon con horas minutos y segundos y devuelve 
 * un json con la equivalencia en grados]
 * 
 */
function ConversorTiempoAGrados(tJson){
	let grados={
			'segundos': 0, 
			'minutos': 0, 
			'horas': 0
			};
	//360º/24h
	grados["horas"]=tJson["horas"]*30;
	//360º/60'
	grados["minutos"]=tJson["minutos"]*6;
	//360º/60"
	grados["segundos"]=tJson["segundos"]*6;

	return grados;
}


/**
 * [devuelve jSon con horas, minutos y segundos]
 * @return {[json]} [description]
 */
function laHoraEs(){
let d = new Date();
	let myJson = {
		'segundos': d.getSeconds(), 
		'minutos': d.getMinutes(), 
		'horas': d.getHours()
	};
return myJson;
}

